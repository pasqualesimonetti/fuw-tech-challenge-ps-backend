FROM php:8.1-apache
RUN a2enmod rewrite
WORKDIR /var/www/html
COPY --chown=www-data:www-data . /var/www/html
