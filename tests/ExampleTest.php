<?php

namespace Tests;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_that_base_endpoint_returns_a_successful_response()
    {
        
        $responseSmi = $this->call('GET','/api/index/smi/');
        $this->assertEquals(200, $responseSmi->getStatusCode());
        
        $responseDax = $this->call('GET','/api/index/dax/');
        $this->assertEquals(200, $responseDax->getStatusCode());
        
        $responseDow = $this->call('GET','/api/index/dow/');
        $this->assertEquals(200, $responseDow->getStatusCode());

        $responseNovartis = $this->call('GET','/api/company/novartis/');
        $this->assertEquals(200, $responseNovartis->getStatusCode());

        $responseBmw = $this->call('GET','/api/company/bmw/');
        $this->assertEquals(200, $responseBmw->getStatusCode());

        $responseApple = $this->call('GET','/api/company/apple/');
        $this->assertEquals(200, $responseApple->getStatusCode());

    }
}
