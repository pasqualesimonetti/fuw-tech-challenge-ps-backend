<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Database;

class DatabaseServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Database::class, function(){
            return new Database;
        });
    }
}