<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\API;

class APIServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(API::class, function(){
            return new API;
        });
    }
}
