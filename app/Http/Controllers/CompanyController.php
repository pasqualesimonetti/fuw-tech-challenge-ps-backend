<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\API;
use App\Services\Database;

class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function showAllCompanies(Database $db, $index = 'smi')
    : JsonResponse {

        $result = $db->getCompaniesByIndex($index);
        return response()->json($result);
    }

    public function showOneCompany(Database $db, API $api, $slug = 'novartis')
    : JsonResponse {

        $company = $db->getCompanyBySlug($slug);
        $quotes = $api->getQuotesByIsin($company['isin']);
        $result = array_merge($company, $quotes);
        return response()->json($result);

    }
    
}