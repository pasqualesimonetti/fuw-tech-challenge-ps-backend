<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'isin',
        'priceYesterday',
        'changePercent',
        'dateLastTrade',
        'changeAbsolute',
        'priceNow',
    ];

    /**
     * The attributes are excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}