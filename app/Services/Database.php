<?php

namespace App\Services;

class Database
{

    private $filepath;
    private $data;

    public function __construct()
    {
        $this->filepath = file(__DIR__ . '../../../mockdata/database-mockdata.csv');
        $this->data = $this->loadData();
    }


    /**
     *
     */
    public function getCompanyBySlug($slug)
    {
        $companies = array_values(array_filter($this->data, function($k) use ($slug) {
            return ($k['slug'] == $slug);
        }));

        if (isset($companies[0])) {
            return $companies[0];
        }
        return null;
    }

    /**
     *
     */
    public function getCompanyByIsin($isin)
    {
        $companies = array_values(array_filter($this->data, function($k) use ($isin) {
            return ($k['isin'] == $isin);
        }));

        if (isset($companies[0])) {
            return $companies[0];
        }
        return null;
    }

    // 
    public function getCompaniesByIndex($index)
    {
        $companies = array_values(array_filter($this->data, function($k) use ($index) {
            return ($k[$index] == 1);
        }));

        if (isset($companies)) {
            return $companies;
        }
        return null;
    }


    private function loadData()
    {
        $lines = $this->filepath;
        $data = [];
        $headers = ['id', 'name', 'isin', 'smi', 'dax', 'dow', 'description'];
        foreach ($lines as $line) {
            $company = array_combine($headers, str_getcsv($line));
            $company['slug'] = str_replace([' ', 'ä', 'ö', 'ü', 'é', '&', '’'], ['-', 'ae', 'oe', 'ue', 'e', '', ''], strtolower($company['name']));
            $data[] = $company;
        }
        return $data;
    }





}
