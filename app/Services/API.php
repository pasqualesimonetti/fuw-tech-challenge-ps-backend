<?php

namespace App\Services;

class API
{

    private $filepath;    
    private $data;

    public function __construct()
    {
        $this->filepath = file(__DIR__ . '../../../mockdata/api-mockdata.csv');
        $this->data = $this->loadData();
    }



    public function getQuotesByIsin($isin)
    {
        $quotes = array_values(array_filter($this->data, function($k) use ($isin) {
            return ($k['isin'] == $isin);
        }));

        if (isset($quotes[0])) {
            return $quotes[0];
        }
        return null;
    }


    public function getQuotesByIndex($indexSlug)
    {
        if ($indexSlug !== 'dax' && $indexSlug !== 'dow') {
            $indexSlug = 'smi';
        }

        $isins['smi'] = ['CH0012221716', 'CH0012138530', 'CH0030170408', 'CH0010645932', 'CH0012214059', 'CH0025751329', 'CH0013841017', 'CH0038863350', 'CH0012005267', 'CH0210483332', 'CH0012032048', 'CH0002497458', 'CH0418792922', 'CH0014852781', 'CH0126881561', 'CH0008742519', 'CH0244767585', 'CH0011075394', 'CH0024608827', 'CH0432492467'];

        $isins['dax'] = ['DE0006599905', 'NL0000235190', 'DE0005190003', 'DE0007664039', 'DE0005140008', 'DE0005557508', 'DE000ENAG999', 'DE0005785802', 'DE000BAY0017', 'DE0008404005', 'DE0007236101', 'DE000BASF111', 'DE0007100000', 'DE000A1EWWW0', 'DE0005810055', 'DE0005552004', 'DE0006047004', 'DE0006048432', 'DE0006231004', 'DE0007037129', 'DE0005785604', 'DE000SYM9999', 'DE0005439004', 'DE0007164600', 'DE000A0HN5C6', 'NL0012169213', 'DE0006969603', 'DE000ZAL1111', 'DE0008430026', 'DE000A1ML7J1', 'DE0006062144', 'DE000A0D9PT0', 'DE000A161408', 'DE000SHL1006', 'DE000A2E4K43', 'DE000ENER6Y0', 'DE000A1DAHH0', 'DE0007165631'];

        $isins['dow'] = ['US46625H1005', 'US0378331005', 'US4781601046', 'US9314271084', 'US5949181045', 'US7427181091', 'US5801351017', 'US1912161007', 'US4581401001', 'US0970231058', 'US1491231015', 'US1667641005', 'US17275R1023', 'US2546871060', 'US4370761029', 'US4592001014', 'US58933Y1055', 'US89417E1091', 'US92343V1044', 'US9311421039', 'US88579Y1010', 'US38141G1040', 'US0311621009', 'US79466L3024', 'US0258161092', 'US6541061031', 'US91324P1021', 'US92826C8394', 'US4385161066', 'US8740541094', 'US2605571031'];


        $quotes = array_values(array_filter($this->data, function($k) use ($isins, $indexSlug) {
            return (in_array($k['isin'], $isins[$indexSlug]));
        }));

        return $quotes;
    }



    private function loadData()
    {
        $lines = $this->filepath;
        $data = [];
        $headers = ['isin', 'priceYesterday'];
        foreach ($lines as $line) {
            $price = array_combine($headers, str_getcsv($line));

            $price['changePercent'] = (mt_rand(0, 300) - 150) / 100;
            $price['dateLastTrade'] = time() - mt_rand(6, 700);
            $price['changeAbsolute'] = round($price['priceYesterday'] * ($price['changePercent'] / 100), 2);
            $price['priceNow'] = $price['priceYesterday'] + $price['changeAbsolute'];

            $data[] = $price;
        }
        return $data;
    }

}
